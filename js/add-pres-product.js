var AdminScreen = window.AdminScreen || {};
var Prescription = {};
var BrandProducts = [];

(function addProductScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });
    
    $(function onDocReady() {

        //Get Prescription Details - From URL?
        var query = decodeURIComponent(window.location.search.substring(1));
        console.log(query);

        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            console.log(item)
            result[item[0]] = decodeURIComponent(item[1])
        });

        var lv_CutitronicsBrandID = result.BrandID;
        Prescription['CutitronicsPresID'] = result.PresID;


        //Set Prescription ID Field value
        $('#PresID').html(Prescription.CutitronicsPresID);


        //Call ajax function to get brand products
        console.log("")
        console.log("Calling getPrescribedProducts with CutitronicsPresID: " + Prescription.CutitronicsPresID);
        console.log("")

        fn_getPrescribedProducts(Prescription.CutitronicsPresID);

        console.log("")
        console.log("Calling getBrandProducts with CutitronicsBrandID: " + lv_CutitronicsBrandID);
        console.log("")

        //Call ajax function to get potential products to add to prescription
        fn_getBrandProducts(lv_CutitronicsBrandID);

    })



    //getPrescribed Products ajax call
    function fn_getPrescribedProducts(lv_CutitronicsPresID){

        $.ajax({
            type: 'GET',
            url: window._config.api.invokeUrl + '/prescriptions/getPrescriptionProducts',
            headers: {
                Authorization: authToken
            },
            success: fn_updatePrescriptionView,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.log("Unable to find any prescribed products for Prescription ID: " + lv_CutitronicsPresID);
            },
            data: {
                CutitronicsPresID: lv_CutitronicsPresID
            }
        })
    }

    function fn_updatePrescriptionView(result) {

        console.log("")
        console.log("getPrescribedProducts return - " + JSON.stringify(result.ServiceOutput));
        console.log("")

        //Get prescription products from API response
        lv_ServiceOutput = result.ServiceOutput;

        lv_PrescriptionProducts = result.ServiceOutput.PresProducts;

        
        //Store and update prescription details
        Prescription.Products = lv_PrescriptionProducts
        Prescription.PrescriptionName = lv_ServiceOutput.PrescriptionName;
        Prescription.SkinType = lv_ServiceOutput.SkinType;

        $('#PresName').html(Prescription.PrescriptionName);
        $('#SkinType').html(Prescription.SkinType);

        //Cache Prescription Items Div before appending products
        lv_ProductDiv = $('#PresItems');
        lv_ProductDiv.html("");


        //Update view with API response values
        
        $('#presItemsAlert').css('display', 'none');

        for(lv_Product of lv_PrescriptionProducts) {
            var lv_PoductHTML = "<div class='row pt-2'> <div class='col'> <div class='card bg-light'> <div class='card-header'> <p class='h4' id='SKU Name'>" + lv_Product.CutitronicsSKUName 
            + "</p> <p id='SKU Code'> <span id='SKUCode' style='font-weight: bold;'>Cutitronics SKU Code: </span>" + lv_Product.CutitronicsSKUCode 
            + " </p> </div> <div class='card-body'> <p class='FrequencyDet'><span id='Frequency' style='font-weight: bold;'>Frequency: </span>  <span id='FrequencyType' style='font-weight: bold;'>Frequency Type: </span>" + lv_Product.FrequencyType 
            + "<span id='PrescriptionType' style='font-weight: bold;'> Prescription Type: </span>" + lv_Product.PrescriptionType 
            + "</p> <p class='UsageDet'><span id='Recommended Amount' style='font-weight: bold;'>Recommended Amount: </span>" + lv_Product.RecommendedAmount 
            + "<br><span id='UsageAM' style='font-weight: bold;'>UsageAM: </span>" + lv_Product.UsageAM + " <span id='UsagePM' style='font-weight: bold;'> UsagePM: </span>" + lv_Product.UsagePM 
            + "</p><p><a class='btn btn-outline-info pl-2' href='/prescription-products.html?" + lv_ServiceOutput.CutitronicsPresID + "|" + lv_ServiceOutput.PrescriptionName + "|" + lv_ServiceOutput.SkinType + "'>Modify Prescription Details</a></p></div></div></div></div>";

            lv_ProductDiv.append(lv_PoductHTML);
        }
    }


    //getBrandProducts ajax call 
    function fn_getBrandProducts(lv_CutitronicsBrandID) {

        $.ajax({
            type: 'GET',
            url: window._config.api.invokeUrl + "/products/getBrandProducts",
            headers: {
                Authorization: authToken
            },
            success: fn_showBrandProducts,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Error getting brand products: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
            },
            data: {
                CutitronicsBrandID: lv_CutitronicsBrandID
            }
        })
    }
    
    function fn_showBrandProducts(result) {

        console.log("")
        console.log("API Brand Product Request Response: " + JSON.stringify(result.ServiceOutput));
        console.log("")


        //Cache accordian to append html
        lv_Accordian = $('#productAccordion');

        lv_BrandProducts = result.ServiceOutput.BrandProducts;
        BrandProducts = result.ServiceOutput.BrandProducts;
        
        lv_Counter = 1;
        console.log("");
        console.log("Brand Products: ");
        console.log(BrandProducts);
        console.log("");

        console.log("Prescription Products: ")
        console.log(Prescription.Products)
        console.log("")


        if (BrandProducts.length == 0) {
            console.log("");
            console.log("No brand products found in back office");
            console.log("");
        } else {
            console.log("")
            console.log("Found brand products in the back office")
            console.log("")
        }

        for(lv_Product of lv_BrandProducts) {
            
            //If product is not in prescribed products then display on view

            var lv_ProductHTML = "<div class='card bg-light pt-2' id='product"+lv_Counter+"'><div class='card-header' id='heading"+lv_Counter+"'>" +
                //Card Header: SKU Name, Code & Desk
                "<p class='h3 card-title' id='Title'>"+ lv_Product.ProductName +
                "</p> <p class='card-text'><span style='font-weight: bold;'>SKU Code: </span><span id='SKUCode' class='pr-5'> " + lv_Product.SKUCode + " </span><br>" +
                "<span style='font-weight: bold;'>Product Description: </span><span id='SKUDesc'> " + lv_Product.TextDescription + " </span></p>" +
                "<button class='btn btn-outline-info' type='button' data-toggle='collapse' data-target='#collapse"+lv_Counter+"' aria-expanded='false' aria-controls='collapse"+lv_Counter+"'> Add Usage Details </button></div>" +
            "<div id='collapse"+lv_Counter+"' class='collapse show' aria-labelledby='heading"+lv_Counter+"' data-parent='#productAccordion'>" +
            "<div class='card-body'>" +
            "<div class='container' id='UsageDetForm"+lv_Counter+"'><div class='form'>"+ "<input type='hidden' id='SKUCode"+lv_Counter + "' value='"+ lv_Product.SKUCode +"'> <input type='hidden' id='PresID"+lv_Counter + "' value='"+ Prescription.CutitronicsPresID +"'> " +
            "<div class='row'><!--Prescription Type--><div class='col'><div class='form-group'><label for='presType"+lv_Counter+"'>Prescription Type:</label><input type='text' class='form-control' id='presType"+lv_Counter+"'></div></div>"+
            "<!--Frequency Type--><div class='col'><div class='form-group'><label for='frequencyType"+lv_Counter+"'>Frequency Type:</label><input type='text' class='form-control' id='frequencyType"+lv_Counter+"'></div></div><!--Frequency--><div class='col'><div class='form-group'>" +
            "<label for='frequency"+lv_Counter+"'>Frequency:</label><input type='number' class='form-control' id='frequency"+lv_Counter+"'></div></div></div><div class='row'><div class='col'><div class='form-group'><label for='usageAM'>Usage AM:</label><input type='number' class='form-control' id='usageAM"+lv_Counter+"'></div>"+
            "<div class='form-group'><label for='usagePM"+lv_Counter+"'>Usage PM:</label><input type='number' class='form-control' id='usagePM"+lv_Counter+"'></div></div><div class='col'><div class='form-group'><label for='recAmount"+lv_Counter+"'>Recommended Amount:</label><input type='text' class='form-control' id='recAmount"+lv_Counter+"'>" +
            "</div><div class='form-group'><br><button id='addPresProd"+lv_Counter+"' class='btn btn-outline-info'>Add Product to Prescription</button> </div></div></div></div></div></div>" +
            "</div>" +
          "</div>" +
          "</div>";
            lv_Accordian.append(lv_ProductHTML);
            lv_Counter = lv_Counter + 1;
        }

        $("#addPresProd1").click(function(){
            fn_handleNewProduct(1);
        });
        $("#addPresProd2").click(function(){
            fn_handleNewProduct(2);
        });
        $("#addPresProd3").click(function(){
            fn_handleNewProduct(3);
        });
        $("#addPresProd4").click(function(){
            fn_handleNewProduct(4);
        });
        $("#addPresProd5").click(function(){
            fn_handleNewProduct(5);
        });
        $("#addPresProd6").click(function(){
            fn_handleNewProduct(6);
        });
        $("#addPresProd7").click(function(){
            fn_handleNewProduct(7);
        });
        $("#addPresProd8").click(function(){
            fn_handleNewProduct(8);
        });

    }

    //Prepare data for adding new prescription item
    function fn_handleNewProduct(lv_ProductCounter) {
        
        $("#product" + lv_ProductCounter).css('display', 'none');

        console.log("#SKUCode" + lv_ProductCounter);
        console.log($("#SKUCode" + lv_ProductCounter).val());

        var lv_UsageDetails = {
            "CutitronicsPresID": $("#PresID" + lv_ProductCounter).val(),
            "CutitronicsSKUCode": $("#SKUCode" + lv_ProductCounter).val(),
            "PrescriptionType": $("#presType" + lv_ProductCounter).val(),
            "Frequency": $("#frequency" + lv_ProductCounter).val(),
            "FrequencyType": $("#frequencyType" + lv_ProductCounter).val(),
            "UsageAM": $("#usageAM" + lv_ProductCounter).val(),
            "UsagePM": $("#usagePM" + lv_ProductCounter).val(),
            "RecommendedAmount": $("#recAmount" + lv_ProductCounter).val()
        }

        fn_addPresProduct(lv_UsageDetails);

    }


    //Ajax call to add a new prescription item
    function fn_addPresProduct(lv_ItemDetails){
        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + '/prescriptions/createPrescriptionItem',
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: completeItemReq,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Unable to add new product to your prescription.");
            },
            data: JSON.stringify(lv_ItemDetails)
        });
    }

    function completeItemReq(result) {
        console.log('API Request Successful: ' + result);
        window.alert('Sucessfully added a new item to your prescription: ' + JSON.stringify(result.ServiceOutput.CutitronicsSKUName))
        fn_getPrescribedProducts(Prescription.CutitronicsPresID);

    }

}(jQuery));