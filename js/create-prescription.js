var AdminScreen = window.AdminScreen || {};

(function createPrescriptionScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });
    

    //Create Brand Prescription API Call
    function createBrandPrescrption(lv_PresDetails) {
        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + "/prescriptions/createBrandPrescription",
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: completeRequest,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed API Request")
                console.error('Error requesting ride: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
                alert('An error occured when requesting your unicorn:\n' + jqXHR.responseText);
            },
            data: JSON.stringify(lv_PresDetails)
        });
    }

    function completeRequest(result) {
        
        console.log('API Response: ');
        console.log(result);

        lv_PrescriptionDet = result.ServiceOutput;

        console.log("Service Output :");
        console.log(lv_PrescriptionDet);

        
        window.location.href = "/add-pres-product.html?" + lv_PrescriptionDet.CutitronicsBrandID + "|" + lv_PrescriptionDet.CutitronicsPresID + "|" + lv_PrescriptionDet.PrescriptionName + "|" + lv_PrescriptionDet.SkinType;
    }

    //Completion handler for getCutitronicsBrand API Call
    function fn_DisplayBrands(result){

        //Get Cutitronics brands from result
        var lv_CutitronicsBrands = result.ServiceOutput.CutitronicsBrands

        var lv_BrandSelectObj = $('#CutitronicsBrandID')

        for(let lv_Brand of lv_CutitronicsBrands) {
            //FIXME Testing only
            console.log(lv_Brand)

            lv_BrandSelectHTML = "<option value='" + lv_Brand.CutitronicsBrandID + "'> " + lv_Brand.BrandName +"  </option>"
            lv_BrandSelectObj.append(lv_BrandSelectHTML);
        }
    }

    $(function onDocReady() {

        var lv_BrandID = 0;

        //Display available brands in select stmt
        $.ajax({
            type: 'GET',
            url: window._config.api.getBrandsURL,
            headers: {
                Authorization: authToken
            },
            success: fn_DisplayBrands,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Error getting cutitronics brands: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
            }
        })

        $('#confirmBrandBtn').on('click', function(event){

            //Get Brand ID and Hide Div
            $('#getBrand').toggle('hide');
            $('#getPresDetails').css('display', 'block');
            
            lv_BrandID = $('#CutitronicsBrandID').val();
            console.log(lv_BrandID);


        });

        $('#presReq').on('click', function(event) {

            event.preventDefault();

            //get values from form 
            var lv_PrescriptionName = $('#PresName').val();
            var lv_SkinType = $('#SkinType').val();

            console.log("API Call Parameters");
            console.log("Pres Name: " + lv_PrescriptionName);
            console.log("Skin Type: " + lv_SkinType);
            console.log("Brand ID: " + lv_BrandID);
            console.log("-------------------");

            //ajax call to create prescription
            if(lv_BrandID != 0) {
                handleProductRequest(event, lv_BrandID);
            } else {
                console.log("Cannot handleProduct request without brand ID");
            }


            //Hide card-body and show brand products
            
        })

    })

    function handleProductRequest(event, lv_BrandID) {

        var lv_PresDetails = {
            "CutitronicsBrandID": lv_BrandID,
            "PrescriptionName": $('#PresName').val(),
            "SkinType": $('#SkinType').val()
        }

        createBrandPrescrption(lv_PresDetails);

    }
    
    
}(jQuery));