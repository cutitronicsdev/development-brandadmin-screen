var AdminScreen = window.AdminScreen || {};

(function viewPrescriptionScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });
    

    //Completion handler for getCutitronicsBrands
    function fn_DisplayBrands(result){

        //Get cutitronics brands from result
        var lv_CutitronicsBrands = result.ServiceOutput.CutitronicsBrands

        var lv_BrandSelectObj = $('#CutitronicsBrandID')

        for(let lv_Brand of lv_CutitronicsBrands) {
            //FIXME Testing only
            console.log(lv_Brand)

            lv_BrandSelectHTML = "<option value='" + lv_Brand.CutitronicsBrandID + "'> " + lv_Brand.BrandName +"  </option>"
            lv_BrandSelectObj.append(lv_BrandSelectHTML);
        }
    }


    $(function onDocReady() {

        //Get cutitronics brands from back office
        $.ajax({
            type: 'GET',
            url: window._config.api.getBrandsURL,
            headers: {
                Authorization: authToken
            },
            success: fn_DisplayBrands,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Error getting cutitronics brands: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
            }
        })

        $('#confirmBrandBtn').on('click', function(event){

            //Get Brand ID and Hide Div
            $('#confirmBrandDiv').toggle('hide');
            
            console.log($('#CutitronicsBrandID').val());

            //Get Prescriptions for Brand
            $.ajax({
                type: 'GET',
                url: window._config.api.invokeUrl + "/prescriptions/getBrandPrescriptions",
                headers: {
                    Authorization: authToken
                },
                success: completeRequest,
                error: function ajaxError(jqXHR, textStatus, errorThrown) {
                    console.error('Error posting firmware: ', textStatus, ', Details: ', errorThrown);
                    console.error('Response: ', jqXHR.responseText);
                    alert('An error occured when calling the API:\n' + jqXHR.responseText);
                },
                data: {
                    CutitronicsBrandID: $('#CutitronicsBrandID').val()
                }
            })
        });

    })
    
    function completeRequest(result){

        console.log("Result: ");
        console.log(result);
        console.log("ServiceOutput: ");
        console.log(result.ServiceOutput.Prescriptions);

        //Get prescription's from response and add to PrescriptionDiv
        lv_Prescriptions = result.ServiceOutput.Prescriptions;
        console.log(typeof(lv_Prescriptions));
        lv_PrescriptionDiv = $('#PrescriptionDiv');

        for(let lv_Prescription of lv_Prescriptions) {
                //Add Prescription as card in new row
                var appendHTML = "<div class='row pt-2'> <div class='col'> <div class='card text-white bg-info'><div class='h5 card-header'>" + lv_Prescription.PrescriptionName 
                +"</div> <div class='card-body'> <p class='card-text'>Prescription ID: " + lv_Prescription.CutitronicsPresID 
                +"</p> <p class='card-text'>Prescription Name: " + lv_Prescription.PrescriptionName 
                + "</p> <p class='card-text'>Skin Type: " + lv_Prescription.SkinType
                + "</p> <p> <a href='/prescription-products.html?"+ lv_Prescription.CutitronicsPresID + "|" + lv_Prescription.PrescriptionName + "|" + lv_Prescription.SkinType  +"' class='btn btn-outline-light'> Manage Prescription Products</a> <button type='submit' class='btn btn-outline-info' data-toggle='modal' data-target='#confirmDelete' id='ModalButton'>Delete Prescription</button> </p></div></div></div>";
                lv_PrescriptionDiv.append(appendHTML);
        }
        
    }
    
}(jQuery));