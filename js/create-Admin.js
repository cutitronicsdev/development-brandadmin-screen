var AdminScreen = window.AdminScreen || {};

(function createAdminUserScopeWrapper($) {
    // var authToken;
    // AdminScreen.authToken.then(function setAuthToken(token) {
    //     if (token) {
    //         authToken = token;
    //     } else {
    //         window.alert("Please Sign in")
    //         window.location.href = '/sign-in.html';
    //     }
    // }).catch(function handleTokenError(error) {
    //     alert(error);
    //     window.location.href = '/sign-in.html';
    // });

    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
            console.log(authToken);
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        console.error(error);
        window.location.href = '/sign-in.html';
    });
    
    $(function onDocReady() {

        // Changed from $('#createAdminID').click(function (e) {...function body ...}
        $('#AdminregistrationForm').submit(function (e) { 
            e.preventDefault();

            var lv_Payload = {};

            lv_Payload.AdminEmail = $('#AdminemailInputRegister').val();
            console.log("This is the email you have provided")
            console.log(lv_Payload.AdminEmail)

            console.log("");
            console.log("Calling fn_createAdmin with the following payload");
            console.log(lv_Payload);
            
            fn_createAdmin(lv_Payload);

            

        });
        


    })

    function fn_createAdmin(lv_Payload) {
        $.ajax({
            method: 'POST',
            //url: 'https://u0cnzylxa4.execute-api.eu-west-2.amazonaws.com/Dev/BrandAdmin/'
            url: "https://u0cnzylxa4.execute-api.eu-west-2.amazonaws.com/Dev/BrandAdmin/admins/createAdmin",
            // url: window._config.api.invokeUrl + '/therapists/createAdmin',
            headers: {

                Authorization: authToken

            },
            dataType: "json",
            contentType: 'application/json',
            success: fn_showNewAdmin,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed to register Admin");
                console.log(jqXHR.responseText);
                console.log(textStatus);
                console.error('Error registering Admin: ', textStatus, ', Details: ', errorThrown);
                console.error(errorThrown);
            },
            data: JSON.stringify(lv_Payload)
        });
        
    }


    function fn_showNewAdmin(result) {

        //Hide ID form
        $('#CreateAdminForm').fadeOut();
        $('#AdminEmail').fadeIn();

        //Get new ID's from API response and cache new ID table body

        var lv_AdminEmail = result.ServiceOutput.AdminEmail;
        var lv_AdminIDTbl = $('#AdminEmailTable');

        //Log API Response
        console.log("")
        console.log("AdminID from API Response:")
        console.log(JSON.stringify(lv_AdminEmail))

        //Display new ID's (option to assign a product?)

        
        lv_AdminIDRow = "<tr>" + 
                                "<td>" + lv_AdminEmail + "</td>"
                            "</tr>"

        lv_AdminIDTbl.append(lv_AdminIDRow);


        // Populate Firmware Select with Available FirmwareID's
    }

  


}(jQuery));