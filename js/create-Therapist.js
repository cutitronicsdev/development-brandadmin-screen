var AdminScreen = window.AdminScreen || {};

(function createTherapistIDScopeWrapper($) {
    // var authToken;
    // AdminScreen.authToken.then(function setAuthToken(token) {
    //     if (token) {
    //         authToken = token;
    //     } else {
    //         window.alert("Please Sign in")
    //         window.location.href = '/sign-in.html';
    //     }
    // }).catch(function handleTokenError(error) {
    //     alert(error);
    //     window.location.href = '/sign-in.html';
    // });

    
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
            console.log(authToken);
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        console.error(error);
        window.location.href = '/sign-in.html';
    });
    
    $(function onDocReady() {

        // Changed from $('#createTherapistID').click(function (e) {...function body ...}
        $('#TherapistregistrationForm').submit(function (e) { 
            e.preventDefault();

            var lv_Payload = {};

            lv_Payload.CutitronicsBrandID = $('#CutitronicsBrandID').val();
            lv_Payload.CutitronicsTherapistID = $('#CutitronicsTherapistID').val();
            lv_Payload.TherapistName = $('#TherapistName').val();
            lv_Payload.TherapistEmail = $('#TherapistemailInputRegister').val();

            console.log("");
            console.log("Calling fn_createTherapist with the following payload");
            console.log(lv_Payload);
            
            fn_createTherapist(lv_Payload);

            

        });
        


    })
    
    function fn_createTherapist(lv_Payload) {
        $.ajax({
            method: 'POST',
            //url: 'https://u0cnzylxa4.execute-api.eu-west-2.amazonaws.com/Dev/BrandAdmin/'
            url: "https://u0cnzylxa4.execute-api.eu-west-2.amazonaws.com/Dev/BrandAdmin/therapists/createTherapist",
            // url: window._config.api.invokeUrl + '/therapists/createTherapist',
            headers: {

                Authorization: authToken

            },
            dataType: "json",
            contentType: 'application/json',
            success: fn_showNewTherapist,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed to register Therapist");
                console.log(jqXHR.responseText);
                console.log(textStatus);
                console.error('Error registering Therapist: ', textStatus, ', Details: ', errorThrown);
                console.error(errorThrown);
            },
            data: JSON.stringify(lv_Payload)
        });
        
    }


    function fn_showNewTherapist(result) {

        //Hide ID form
        $('#CreateTherapistForm').fadeOut();
        $('#TherapistIDs').fadeIn();

        //Get new ID's from API response and cache new ID table body
        var lv_TherapistIDTbl = $('#TherapistIDTable');
        var lv_TherapistID = result.ServiceOutput.CutitronicsTherapistID;
        var lv_BrandID = result.ServiceOutput.CutitronicsBrandID;
        var lv_TherapistName = result.ServiceOutput.TherapistName;

        //Log API Response
        console.log("")
        console.log("TherapistID from API Response:")
        console.log(JSON.stringify(lv_TherapistID))

        //Display new ID's (option to assign a product?)

        
        lv_TherapistIDRow = "<tr>" + 
                                "<td>" + lv_TherapistID + "</td>"
                                "<td>" + lv_TherapistName + "</td>"
                                "<td>" + lv_BrandID + "</td>"
                            "</tr>"

        lv_TherapistIDTbl.append(lv_TherapistIDRow);


        // Populate Firmware Select with Available FirmwareID's
    }

  
}(jQuery));