var AdminScreen = window.AdminScreen || {};
var v_CutitronicsBrandID = "";
var v_DeleteQKey = "";

(function viewProductsScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });
    
    $(function onDocReady() {

        $('#confirmBrandBtn').on('click', function(event){

            //Get Brand ID and Hide Div
            $('#confirmBrandDiv').toggle('hide');
            
            console.log($('#CutitronicsBrandID').val());

            v_CutitronicsBrandID = $('#CutitronicsBrandID').val();

            //Get question set for Brand
            $.ajax({
                type: 'GET',
                url: window._config.api.invokeUrl + "/HAYGOQuestions/getBrandQuestionSet",
                headers: {
                    Authorization: authToken
                },
                success: fn_fillQSetSelect,
                error: function ajaxError(jqXHR, textStatus, errorThrown) {
                    console.error('Response: ', jqXHR.responseText);
                    alert('An error occured when calling the API:\n' + jqXHR.responseText);
                },
                data: {
                    CutitronicsBrandID: $('#CutitronicsBrandID').val()
                }
            })
        });
    })


    function fn_fillQSetSelect(result){

        console.log("");
        console.log("Successfully got question sets from back office");
        console.log("Result:" + JSON.stringify(result));

        
        lv_QSetSelect = $('#QuestionSet');

        lv_QuestionSets = result.ServiceOutput.QuestionSets;

        console.log("Question Sets FROM API");
        console.log(lv_QuestionSets);
        console.log("");

        for(let lv_QuestionIndex in lv_QuestionSets){
            
            console.log("Question set with index");
            console.log(lv_QuestionSets[lv_QuestionIndex]);
        
            lv_SelectHTML = "<option value='" + lv_QuestionSets[lv_QuestionIndex] + "'> " + lv_QuestionSets[lv_QuestionIndex] + " </option>";

            console.log("Select HTML" + lv_SelectHTML);

            lv_QSetSelect.append(lv_SelectHTML);
        }

        //Show Select
        $('#QuestionSelect').fadeIn();

    }

    $('#getQuestionSet').click(function (e) { 
        e.preventDefault();
        
        //Get payload for getQuestions
        if($('#QuestionSet').val() == "Default"){
            alert("Please select a question set to view");
        } else {

            var lv_SpinnerHTML = "<div class='spinner-border text-primary' role='status'></div>";

            //Reset views 
            $('#title-card-body').html(lv_SpinnerHTML);
            $('#nav-tab').html("");
            $('#nav-tabContent').html(lv_SpinnerHTML);
            $('#QuestionTable').html("");
            fn_getHaygoQuestions(v_CutitronicsBrandID, $('#QuestionSet').val())
        }

    });

    function fn_getHaygoQuestions(lv_CutitronicsBrandID, lv_QuestionSet) {
        //Get products for Brand
        $.ajax({
            type: 'GET',
            url: window._config.api.invokeUrl + "/HAYGOQuestions/getHaygoQuestions",
            headers: {
                Authorization: authToken
            },
            success: completeRequest,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Response: ', jqXHR.responseText);
                alert('An error occured when calling the API:\n' + jqXHR.responseText);
            },
            data: {
                "CutitronicsBrandID": lv_CutitronicsBrandID, 
                "QuestionSet": lv_QuestionSet
            }
        })
    }


    function completeRequest(result){
        
        console.log("");
        console.log("Successfully Completed Execution: ");
        console.log("");

        //1. Get HTML Elements from DOM
        var lv_NavTab = $('#nav-tab');
        var lv_NavTabContent = $('#nav-tabContent');
        var lv_QuestionTable = $('#QuestionTable');

        //2. Get Data from API Response
        var lv_CutitronicsBrandID = result.ServiceOutput.CutitronicsBrandID;
        var lv_Questions = result.ServiceOutput.Questions;

        console.log("");
        console.log("Questions from /getHaygoQuestions for brand: " + result.ServiceOutput.CutitronicsBrandID);
        console.log("Type: " + typeof result.ServiceOutput.Questions);
        console.log("Questions: " + JSON.stringify(result.ServiceOutput.Questions));
        console.log("");

        //Turn off spinners
        $('#title-card-body').html("");
        $('#nav-tabContent').html("");
        
        //Show create new question button
        var lv_CardBody = $('#title-card-body');
        var lv_NewQBtnHTML = "<a href='/create-question.html?BrandID=" + lv_CutitronicsBrandID + "' id='NewQBtn' class='btn btn-outline-primary mb-2'>Create new question</a>";
        lv_CardBody.append(lv_NewQBtnHTML);
        lv_CardBody.show();

        //For Each Question
        for(let lv_Question in lv_Questions) {

            console.log("")
            console.log("Question: " + JSON.stringify(lv_Question));
            console.log("");

            var lv_NavTabHTML = "";

            //If first element show on load
            if(lv_Question == 0){
                //3. Build String to Append to nav-tab
                lv_NavTabHTML = "<button class='nav-link active' id='nav-tab-" + lv_Questions[lv_Question].QuestionKey + "' data-bs-toggle='tab' data-bs-target='#nav-" + lv_Questions[lv_Question].QuestionKey + "' type='button' role='tab' aria-controls='nav-" + lv_Questions[lv_Question].QuestionKey + "' aria-selected='true'>" + lv_Questions[lv_Question].QuestionText + "</button>";

                //4. Build String to Append to nav-tabContent
                lv_NavTabContentHTML = "<div class='tab-pane fade show active' id='nav-" + lv_Questions[lv_Question].QuestionKey + "' role='tabpanel' aria-labelledby='nav-tab-" + lv_Questions[lv_Question].QuestionKey + "'>" +
                                        "<p class='pt-3'>Percentage of customers that voted 1:</p>" +
                                        "<div class='progress'>" +
                                        "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['1'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['1'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['1'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                        "</div>" +
                                        "<p>Percentage of customers that voted 2:</p>" +
                                        "<div class='progress'>" +
                                        "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['2'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['2'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['2'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                        "</div>" +
                                        "<p>Percentage of customers that voted 3:</p>" +
                                        "<div class='progress'>" +
                                        "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['3'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['3'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['3'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                        "</div>" +
                                        "<p>Percentage of customers that voted 4:</p>" +
                                        "<div class='progress'>" +
                                        "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['4'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['4'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['4'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                        "</div>" +
                                        "<p>Percentage of customers that voted 5:</p>" +
                                        "<div class='progress'>" +
                                        "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['5'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['5'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['5'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" + 
                                        "</div>" + 
                                    "</div>";

            } else {

                //3. Build String to Append to nav-tab
                lv_NavTabHTML = "<button class='nav-link' id='nav-tab-" + lv_Questions[lv_Question].QuestionKey + "' data-bs-toggle='tab' data-bs-target='#nav-" + lv_Questions[lv_Question].QuestionKey + "' type='button' role='tab' aria-controls='nav-" + lv_Questions[lv_Question].QuestionKey + "' aria-selected='false'>" + lv_Questions[lv_Question].QuestionText + "</button>";

                //4. Build String to Append to nav-tabContent
                lv_NavTabContentHTML = "<div class='tab-pane fade' id='nav-" + lv_Questions[lv_Question].QuestionKey + "' role='tabpanel' aria-labelledby='nav-tab-" + lv_Questions[lv_Question].QuestionKey + "'>" +
                                            "<p class='pt-3'>Percentage of customers that voted 1:</p>" +
                                            "<div class='progress'>" +
                                            "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['1'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['1'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['1'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                            "</div>" +
                                            "<p>Percentage of customers that voted 2:</p>" +
                                            "<div class='progress'>" +
                                            "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['2'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['2'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['2'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                            "</div>" +
                                            "<p>Percentage of customers that voted 3:</p>" +
                                            "<div class='progress'>" +
                                            "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['3'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['3'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['3'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                            "</div>" +
                                            "<p>Percentage of customers that voted 4:</p>" +
                                            "<div class='progress'>" +
                                            "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['4'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['4'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['4'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                            "</div>" +
                                            "<p>Percentage of customers that voted 5:</p>" +
                                            "<div class='progress'>" +
                                                "<div class='progress-bar' role='progressbar' style='width: " + Math.round((lv_Questions[lv_Question].FeedbackScore['5'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%' aria-valuenow='" + Math.round((lv_Questions[lv_Question].FeedbackScore['5'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "' aria-valuemin='0' aria-valuemax='100'>" + Math.round((lv_Questions[lv_Question].FeedbackScore['5'] / lv_Questions[lv_Question].FeedbackScore['Total']) * 100) + "%</div>" +
                                            "</div>" + 
                                        "</div>";
            }

            //5.TODO Build String to Append to Question Table
            lv_QuestionTableHTML = "<tr>" +
                                        "<td>" + lv_Questions[lv_Question].QuestionText + "</td>" +
                                        "<td>" + lv_Questions[lv_Question].FeedbackScore['Total'] + "</td>" +
                                        "<td>" +
                                        "<span>" +
                                        "<a class='btn btn-outline-primary' href='/modify-question.html?BrandID=" + lv_CutitronicsBrandID + "&QKey=" + lv_Questions[lv_Question].QuestionKey + "'&QText=" + lv_Questions[lv_Question].QuestionText + ">Edit</a>" +
                                        "</span>" +
                                        "</td>" +
                                        "<td>" +
                                        "<span>" +
                                        "<button type='button' class='btn btn-outline-primary' data-toggle='modal' data-target='#exampleModalCenter' id='"+  lv_Questions[lv_Question].QuestionKey +"'>Delete</button>" +
                                        "</span>" +
                                        "</td>" +
                                    "</tr>";

            //6. Append all build Strings to DOM elements
            lv_NavTab.append(lv_NavTabHTML);
            lv_NavTabContent.append(lv_NavTabContentHTML);
            lv_QuestionTable.append(lv_QuestionTableHTML);


            //Add listener to delete button
            $("#" + lv_Questions[lv_Question].QuestionKey + "").click(function (e) { 
                e.preventDefault();
                v_DeleteQKey = lv_Questions[lv_Question].QuestionKey;

                var v_Confirm = confirm("Are you sure you want to delete this question?")

                lv_Payload = {
                    "CutitronicsBrandID": v_CutitronicsBrandID,
                    "QuestionKey": v_DeleteQKey
                }

                //Ajax delete call
                if (v_Confirm == true) {
                    $.ajax({
                        method: 'POST',
                        url: window._config.api.invokeUrl + "/HAYGOQuestions/deleteQuestion",
                        headers: {
                            Authorization: authToken
                        },
                        dataType: "json",
                        contentType: 'application/json',
                        success: fn_completeDelete,
                        error: function ajaxError(jqXHR, textStatus, errorThrown) {
                            window.alert("Failed to create new question, try again");
                            console.error('Response: ', jqXHR.responseText);
                        },
                        data: JSON.stringify(lv_Payload)
                    });
                }

                console.log("");
                console.log("Delete key set to '" + v_DeleteQKey + "'");
                console.log("");
            });

            
            //Show Question Table
            $('#QuestionDiv').fadeIn();
        }
    }
    

    function fn_completeDelete(result) {
        window.alert("Successfully deleted question from set.");
        window.location.href = "/HAYGO-Questions.html"
    }
    
}(jQuery));