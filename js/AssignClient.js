var AdminScreen = window.AdminScreen || {};
var Globals = {};

(function AssignClientIDScopeWrapper($) {
    // var authToken;
    // AdminScreen.authToken.then(function setAuthToken(token) {
    //     if (token) {
    //         authToken = token;
    //     } else {
    //         window.alert("Please Sign in")
    //         window.location.href = '/sign-in.html';
    //     }
    // }).catch(function handleTokenError(error) {
    //     alert(error);
    //     window.location.href = '/sign-in.html';
    // });

    
   /* var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
            console.log(authToken);
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        console.error(error);
        window.location.href = '/sign-in.html';
    }); */
    


      // Idea for passing in a parameter to ajax call if it is implemented as  a function
         //$("#CutitronicsBrandID").change(function(){
                //fn_getAllGuests($('#CutitronicsBrandID').val()); REMOVE IF NOT NEEDED

    $(function onDocReady() {

        // Changed from $('#createTherapistID').click(function (e) {...function body ...}
        // Get AllClients by passing in CutitronicsBrandID from Form.

        // The rest is not very important right now.
        $('#AssignTherapistsForm').submit(function (e) { 
            e.preventDefault();

            var lv_Payload = {};
            
            lv_Payload.CutitronicsBrandID = $('#CutitronicsBrandID').val();
            lv_Payload.CutitronicsTherapistID = $('#CutitronicsTherapistID').val();
            lv_Payload.CutitronicsClientID = $('#CutitronicsClientID').val();

            console.log("");
            console.log("Calling fn_createTherapist with the following payload");
            console.log(lv_Payload);
            

            fn_associateTherapist_Client(lv_Payload);

            

        });
        


    })
        


    function fn_associateTherapist_Client(lv_Payload) {
        $.ajax({
            method: 'POST',
            //url: 'https://xoky1iyt8d.execute-api.eu-west-2.amazonaws.com/Dev/BrandAdmin',
            url: window._config.api.invokeUrl + '/therapists/associateTherapist_Client',
            /*headers: {
                Authorization: authToken
            }, */
            dataType: "json",
            contentType: 'application/json',
            success: fn_showNewTherapist,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
                console.log(textStatus);
                console.error('Error associating  Therapists with Client: ', textStatus, ', Details: ', errorThrown);
                console.error(errorThrown);
            },
            data: JSON.stringify(lv_Payload)
        });
        
    }


    function fn_showNewTherapist(result) {

        //Hide ID form
        $('#CreateTherapistForm').fadeOut();
        $('#TherapistIDs').fadeIn();

        //Get new ID's from API response and cache new ID table body
        var lv_TherapistIDTbl = $('#TherapistIDTable');
        var lv_TherapistIDs = $('#CutitronicsTherapistID').val() ;
        var lv_BrandID = result.ServiceOutput.CutitronicsBrandID;

        //Log API Response
        console.log("")
        console.log("TherapistID from API Response:")
        console.log(JSON.stringify(lv_TherapistIDs  ))

        //Display new ID's (option to assign a product?)

        for (let therapistid in lv_TherapistIDs) {
        
        lv_TherapistIDRow = "<tr>" + 
                                "<td>" + lv_TherapistIDs[therapistid] + "</td>"
                                "<td>" + lv_BrandID + "</td>"
                            "</tr>"

        lv_TherapistIDTbl.append(lv_TherapistIDRow);

        }
    }



}(jQuery));