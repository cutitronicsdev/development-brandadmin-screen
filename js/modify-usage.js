var AdminScreen = window.AdminScreen || {};
var ItemGuidance = {};

(function addProductScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });

    function fn_getPresItemGuidance(lv_GetGuidanceIn) {

        //Call /getPresItemUsage API Endpoint 
        $.ajax({
            type: 'GET',
            url: window._config.api.invokeUrl + '/prescriptions/getPresItemUsage',
            headers: {
                Authorization: authToken
            },
            success: fn_DisplayUsageForm,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Error getting item guidance: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
                alert('An error occured when calling the API:\n' + jqXHR.responseText);
            },
            data: {
                CutitronicsPresID: lv_GetGuidanceIn.CutitronicsPresID,
                CutitronicsPresDetID: lv_GetGuidanceIn.CutitronicsPresDetID,
                CutitronicsSKUCode: lv_GetGuidanceIn.CutitronicsSKUCode
            }
        })
    }

    function fn_DisplayUsageForm(result) {
        //Change placeholders in display usage form
        ItemGuidance.PrescriptionType = result.ServiceOutput.PrescriptionType
        ItemGuidance.Frequency = result.ServiceOutput.Frequency
        ItemGuidance.FrequencyType = result.ServiceOutput.FrequencyType
        ItemGuidance.UsageAM = result.ServiceOutput.UsageAM
        ItemGuidance.UsagePM = result.ServiceOutput.UsagePM
        ItemGuidance.RecommendedAmount = result.ServiceOutput.RecommendedAmount

        //Change usage form placeholders
        $('#presType').attr('placeholder', result.ServiceOutput.PrescriptionType);
        $('#frequency').attr('placeholder', result.ServiceOutput.Frequency);
        $('#frequencyType').attr('placeholder', result.ServiceOutput.FrequencyType);
        $('#usageAM').attr('placeholder', result.ServiceOutput.UsageAM);
        $('#usagePM').attr('placeholder', result.ServiceOutput.UsagePM);
        $('#recAmount').attr('placeholder', result.ServiceOutput.RecommendedAmount);

        //Hidden Values for Modify Function 
        $('#HiddenPresID').attr('value', result.ServiceOutput.CutitronicsPresHDRID);
        $('#HiddenPresDetID').attr('value', result.ServiceOutput.CutitronicsPresDETID);
        $('#HiddenSKUCode').attr('value', result.ServiceOutput.CutitronicsSKUCode);
    }

    function fn_UpdatePresItemGuidance(lv_UpdateGuidanceInput) {
        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + '/prescriptions/updateItemGuidance',
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: fn_completeGuidanceUpdate,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed API Request: Update Guidance");
                console.error('Error Response: ', jqXHR.responseText);
            },
            data: JSON.stringify(lv_UpdateGuidanceInput)
        });
    }

    function fn_completeGuidanceUpdate(result) {
        console.log("Completed API Request");
        console.log("Result" + result);
        window.location.href = "/prescription-products.html?"+ result.ServiceOutput.CutitronicsPresHDRID + "|" + result.ServiceOutput.PrescriptionName + "|" + result.ServiceOutput.SkinType;
    }
    
    $(function onDocReady() {

        //Get Prescription Details - From URL?

        var query = decodeURIComponent(window.location.search.substring(1));
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });
        

        console.log("Prescription Name: " + result.PresName);
        console.log("Prescription ID: " + result.PresID);
        console.log("CutitronicsSKUCode: " + result.SKUCode);
        console.log("PrescriptionDETID: " + result.PresDetID);

        //Display Prescription Details

        $('#PresID').html(result.PresID);
        $('#PresName').html(result.PresName);
        $('#SKUCode').html(result.SKUCode);
        $('#PresDetID').html(result.SKUCode);
        
        //Prepare Data for get Default guidance ajax call

        lv_GetGuidanceIn = {
            "CutitronicsPresID": result.PresID,
            "CutitronicsPresDetID": result.PresDetID,
            "CutitronicsSKUCode": result.SKUCode
        }

        // Get Item Guidance and Insert into input placeholders
        fn_getPresItemGuidance(lv_GetGuidanceIn);

        $('#UsageDetForm').submit(function (e) { 
            e.preventDefault();
            
            lv_UpdateGuidanceInput = {
                "CutitronicsPresHDRID": $('#HiddenPresID').val(),
                "CutitronicsPresDETID": $('#HiddenPresDetID').val(),
                "CutitronicsSKUCode": $('#HiddenSKUCode').val()
            }

            //Get form values if they have changed
            if($('#presType').val().trim().length === 0) {
                lv_UpdateGuidanceInput.PrescriptionType = ItemGuidance.PrescriptionType;
            } else {
                lv_UpdateGuidanceInput.PrescriptionType = $('#presType').val();
            }
            
            if($('#frequency').val().trim().length === 0) {
                lv_UpdateGuidanceInput.Frequency = ItemGuidance.Frequency;
            } else {
                lv_UpdateGuidanceInput.Frequency = $('#frequency').val();
            }

            if($('#frequencyType').val().trim().length === 0) {
                lv_UpdateGuidanceInput.FrequencyType = ItemGuidance.FrequencyType;
            } else {
                lv_UpdateGuidanceInput.FrequencyType = $('#frequencyType').val();
            }

            if($('#usageAM').val().trim().length === 0) {
                lv_UpdateGuidanceInput.UsageAM = ItemGuidance.UsageAM;
            } else {
                lv_UpdateGuidanceInput.UsageAM = $('#usageAM').val();
            }

            if($('#usagePM').val().trim().length === 0) {
                lv_UpdateGuidanceInput.UsagePM = ItemGuidance.UsagePM;
            } else {
                lv_UpdateGuidanceInput.UsagePM = $('#usagePM').val();
            }

            if($('#recAmount').val().trim().length === 0) {
                lv_UpdateGuidanceInput.RecommendedAmount = ItemGuidance.RecommendedAmount;
            } else {
                lv_UpdateGuidanceInput.RecommendedAmount = $('#recAmount').val();
            }

            console.log("")
            console.log("Callinf fn_UpdatePresItemGuidance with input:")
            console.log(lv_UpdateGuidanceInput)
            console.log("")

            //Call Ajax funciton to update pres Item
            fn_UpdatePresItemGuidance(lv_UpdateGuidanceInput);
        });
    })


  
}(jQuery));