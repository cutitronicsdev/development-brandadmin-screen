var AdminScreen = window.AdminScreen || {};

(function viewProductsScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });
    
    $(function onDocReady() {

        $('#confirmBrandBtn').on('click', function(event){

            //Get Brand ID and Hide Div
            $('#confirmBrandDiv').toggle('hide');
            
            console.log($('#CutitronicsBrandID').val());

            //Get products for Brand
            $.ajax({
                type: 'GET',
                url: window._config.api.invokeUrl + "/products/getBrandProducts",
                headers: {
                    Authorization: authToken
                },
                success: completeRequest,
                error: function ajaxError(jqXHR, textStatus, errorThrown) {
                    console.error('Response: ', jqXHR.responseText);
                    alert('An error occured when calling the API:\n' + jqXHR.responseText);
                },
                data: {
                    CutitronicsBrandID: $('#CutitronicsBrandID').val()
                }
            })
        });
        
    })

    function completeRequest(result){
        //console.log("Successful API Request " + result.ServiceOutput);

        //Get products from API response 
        var lv_BrandProducts = result.ServiceOutput.BrandProducts;

        //Add each product to product div
        var lv_ProductDiv = $('#productDiv');


        for (let lv_Product of lv_BrandProducts) {
            console.log(lv_Product);
            lv_ProductHTML = "<div class='row pt-2'>" + 
                                "<div class='col'>" +
                                "<div class='card bg-light'>" +
                                    "<div class='card-header'>" +
                                        "<div class='card-title h2'>" + lv_Product.ProductName + " </div>" +
                                        "<div class='card-text'>" + lv_Product.TextDescription + " </div>" +
                                    "</div>" +
                                    "<div class='card-body'>" +
                                        "<div class='container'>" +
                                            "<div class='row pt-3' id='SKUDet'>" +
                                            "<div class='col'>" +
                                                "<div><span class='card-text h6'>SKU Code: </span> <span class='card-text' id='SKUCode'> " + lv_Product.SKUCode + " </span> </div>" +
                                            "</div>" +
                                            "<div class='col'>" +
                                                "<div><span class='card-text h6'>SKU Type: </span> <span class='card-text' id='SKUType'> " + lv_Product.Type + " </span></div>" +
                                            "</div>" +
                                            "<div class='col'>" +
                                                "<div><span class='card-text h6'>SKU Category: </span> <span class='card-text' id='SKUCat'> " + lv_Product.Category + " </span> </div>" +
                                            "</div>" +
                                            "</div>" +
                                            "<div class='row pt-3'>" +
                                            "<div class='col'>" +
                                                "<div> <span class='card-text h6'>Default URL: </span> <span class='card-text' id='ProdURL'>" + lv_Product.DefaultURL + "</span></div>" +
                                            "</div>" +
                                            "<div class='col'>" +
                                                "<div> <span class='card-text h6'>Default Image: </span><span class='card-text' id='ProdIMG'>" + lv_Product.ImgURL +"</span></div>" +
                                            "</div>" +
                                            "<div class='col'>" +
                                                "<div> <span class='card-text h6'>Default Video: </span> <span class='card-text' id='ProdVid'>" + lv_Product.VideoURL + "</span></div>" +
                                            "</div>" +
                                            "</div>" +
                                            "<div class='row pt-3'>" +
                                            "<div class='col'>" +
                                                "<div> <span class='card-text h6'>Default Volume: </span><span class='card-text' id='Volume'> " + lv_Product.defaultVolume + "</span></div>" +
                                            "</div>" +
                                            "<div class='col'>" +
                                                "<div> <span class='card-text h6'>Recommended Amount: </span> <span class='card-text' id='RecAmount'>" + lv_Product.RecommendedAmount + "</span></div>" +
                                            "</div>" +
                                            "<div class='col'>" +
                                                "<div> <span class='card-text h6'> Default Parameters: </span> <span class='card-text' id='Parameters'>" + lv_Product.DefaultParameters + "</span></div>" +
                                            "</div>" +
                                            "</div>" +
                                            "<div class='row justify-content-center pt-2'>" +
                                            "<div class='col justify-content-center'>" +
                                                "<a class='btn btn-outline-info' href='/modify-product.html?BrandID=" + result.ServiceOutput.CutitronicsBrandID + "&SKUCode=" + lv_Product.SKUCode + "'>Edit Product</a>" +
                                            "</div>" +
                                            "<div class='col justify-content-center'>" +
                                                "<button type='button' class='btn btn-outline-info' data-toggle='modal' data-target='#exampleModal' value='"+ lv_Product.SKUCode +"'>Delete Product</button>" +
                                            "</div>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                                "</div>" +
                            "</div>";
            
            lv_ProductDiv.append(lv_ProductHTML);
        }


    }
    
    
}(jQuery));